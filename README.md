# HTML Crawler

Designed to parse .html files and to detect some specified elements, even after some minor changes

---

## Getting Started
### Dependencies

1. Jsoup 1.11.13
2. Mockito 1.9.5
3. JUnit 4.12

### Running Executable Jar
To run html-crawler-1.0-SNAPSHOT.jar you should pass enter this command:

"java -jar html-crawler-1.0-SNAPSHOT.jar <input_origin_file_path> <input_other_sample_file_path> <original_element_id>"

### Building Project
To build project simply run "gradlew.bat build" command and it will automatically
build project with all dependencies.
### Running Tests
To run tests simply run "gradlew.bat test -i"