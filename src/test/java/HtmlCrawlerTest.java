import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import util.JsoupWrapper;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HtmlCrawlerTest {
    private static final String ORIGINAL_FILENAME = "/original.html";
    private static final String MODIFIED_FILENAME = "/modified.html";
    private static final String ORIGINAL_ELEMENT_ID = "default-id";
    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final int DEFAULT_ELEMENTS_CAPACITY = 10;
    private static final String SEARCH_ATTRIBUTE_KEY = "match";
    private static final String SEARCH_ATTRIBUTE_VALUE = "matchingValue";
    private static final String DEFAULT_ORIGINAL_TAG = "tag";
    public static final String DEFAULT_HIERARCHY_SEACH_QUERY = "parent tag";

    @Mock
    private JsoupWrapper jsoupWrapper;
    @Mock
    private Document document;

    private Element originalElement;
    private Elements modifiedCandidates;

    @InjectMocks
    private HtmlCrawler testEntry;

    @Before
    public void setUp() throws Exception {
        prepareOriginalElement();
        prepareModifiedCandidates();
        when(jsoupWrapper.parse(ORIGINAL_FILENAME, DEFAULT_ENCODING)).thenReturn(document);
        when(jsoupWrapper.parse(MODIFIED_FILENAME, DEFAULT_ENCODING)).thenReturn(document);
        when(document.select(DEFAULT_HIERARCHY_SEACH_QUERY)).thenReturn(modifiedCandidates);
        when(document.getElementById(ORIGINAL_ELEMENT_ID)).thenReturn(originalElement);
    }

    @Test
    public void shouldParseDocumentWithSpecifiedFilename() throws IOException {
        testEntry.getElementFromOriginalFile(ORIGINAL_FILENAME, ORIGINAL_ELEMENT_ID);
        verify(jsoupWrapper).parse(ORIGINAL_FILENAME, DEFAULT_ENCODING);
    }

    @Test
    public void shouldReturnSelectedElementFromDocument() throws IOException {
        Element resultElement = testEntry.getElementFromOriginalFile(ORIGINAL_FILENAME, ORIGINAL_ELEMENT_ID);
        assertEquals(originalElement, resultElement);
    }

    @Test
    public void shouldReturnModifiedCandidatesFromDocument() throws IOException {
        Elements resultElements = testEntry.getModifiedCandidates(MODIFIED_FILENAME, originalElement);
        assertEquals(modifiedCandidates, resultElements);
    }

    @Test
    public void shouldReturnMostSuitableCandidate() {
        String searchPath = "matching tag";
        prepareMatchingCandidate(searchPath);
        String resultPath = testEntry.findModifiedElementPath(modifiedCandidates, originalElement);
        assertEquals(searchPath, resultPath);
    }


    private void prepareModifiedCandidates() {
        modifiedCandidates = new Elements(DEFAULT_ELEMENTS_CAPACITY);
        modifiedCandidates.add(new Element("span"));
        modifiedCandidates.add(new Element("div"));
    }

    private void prepareOriginalElement() {
        originalElement = new Element(DEFAULT_ORIGINAL_TAG);
        originalElement.attr(SEARCH_ATTRIBUTE_KEY, SEARCH_ATTRIBUTE_VALUE);
        new Element("parent")       //adding parent elements hierarchy to our original element
                .appendChild(new Element("childParent")
                        .appendChild(originalElement));
    }

    private void prepareMatchingCandidate(String candidatePath) {
        Element matchingCandidate = new Element(candidatePath);
        matchingCandidate.attr(SEARCH_ATTRIBUTE_KEY, SEARCH_ATTRIBUTE_VALUE);
        modifiedCandidates.add(matchingCandidate);
    }
}