import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import util.JsoupWrapper;

import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        String originalFileName = args[0];
        String modifiedFileName = args[1];
        String elementId = args[2];

        JsoupWrapper jsoupWrapper = new JsoupWrapper();
        HtmlCrawler crawler = new HtmlCrawler(jsoupWrapper);
        Element originalElement = crawler.getElementFromOriginalFile(originalFileName, elementId);
        Elements modifiedCandidates = crawler.getModifiedCandidates(modifiedFileName, originalElement);
        String resultPath = crawler.findModifiedElementPath(modifiedCandidates, originalElement);

        System.out.println(resultPath);
    }
}
