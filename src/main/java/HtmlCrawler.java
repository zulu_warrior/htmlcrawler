import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import util.JsoupWrapper;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class HtmlCrawler {

    private JsoupWrapper jsoupWrapper;

    public HtmlCrawler(JsoupWrapper jsoupWrapper) {
        this.jsoupWrapper = jsoupWrapper;
    }

    public Element getElementFromOriginalFile(String filename, String elementId) throws IOException {
        return jsoupWrapper.parse(filename, "UTF-8").getElementById(elementId);
    }

    public Elements getModifiedCandidates(String modifiedFileName, Element originalElement) throws IOException {
        String selectQuery = composeSearchQueryFromOriginalElement(originalElement);
        return jsoupWrapper.parse(modifiedFileName, "UTF-8").select(selectQuery);
    }

    public String findModifiedElementPath(Elements modifiedCandidates, Element originalElement) {
        return modifiedCandidates.stream()
                .map(compareModifiedWithOriginal(originalElement))
                .max(Comparator.comparingLong(Entry::getMatches))
                .map(e -> e.getElement().cssSelector())
                .orElse("");
    }

    private String composeSearchQueryFromOriginalElement(Element originalElement) {
        String parentSearchPath = originalElement.parent().parent().cssSelector();  //assuming, that changes will not affect higher hierarchies
                                                                                    // we could possibly retrieve all elements, that would increase complexity
        String originalSearchTag = originalElement.tagName();
        return parentSearchPath + " " + originalSearchTag;
    }

    private Function<Element, Entry> compareModifiedWithOriginal(Element original) {
        return (modified) -> {
            List<Attribute> originalAttributes = original.attributes().asList();
            List<Attribute> modifiedAttributes = modified.attributes().asList();
            long matches = originalAttributes.stream()
                    .filter(modifiedAttributes::contains)
                    .count();
            return new Entry(modified, matches);
        };
    }

    private class Entry {
        private Element element;
        private long matches;

        public Entry(Element element, long matches) {
            this.element = element;
            this.matches = matches;
        }

        public Element getElement() {
            return element;
        }

        public long getMatches() {
            return matches;
        }
    }
}
