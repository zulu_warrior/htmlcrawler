package util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;

public class JsoupWrapper {     //for testing purposes
    public Document parse(String filename, String encoding) throws IOException {
        return Jsoup.parse(new File(filename), encoding);
    }
}
